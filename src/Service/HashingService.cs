using System;
using System.Security.Cryptography;
using System.Text;
using APITemplate.Core.Interface;
using AutoMapper;

namespace APITemplate.Service
{
    public class HashingService : IHashingService
    {
        public byte[] Hash(byte[] salt, string passwordToHash)
        {
            using (var hmac = new HMACSHA512(salt))
            {
                return hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordToHash));
            }
        }

        public byte[] GenerateSalt()
        {
            using (var hmac = new HMACSHA512())
            {
                return hmac.Key;
            }
        }

        public string GenerateToken(int length = 32)
        {
            using (var provider = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[length];
                provider.GetBytes(bytes);

                return Convert.ToBase64String(bytes);
            }
        }

        public bool VerifyHash(string verify, byte[] salt, byte[] actual)
        {
            using (var hmac = new HMACSHA512(salt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(verify));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != actual[i])
                        return false;
                }
                return true;
            }
        }
    }
}