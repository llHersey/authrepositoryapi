using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using APITemplate.Core.Interface;
using APITemplate.Core.Model;
using Microsoft.Extensions.Options;

namespace APITemplate.Service
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings emailSettings;
        public EmailService(IOptionsSnapshot<EmailSettings> emailSettings)
        {
            this.emailSettings = emailSettings.Value;
        }

        public Task Send(string sendTo, string subject, string body, bool isBodyHtml)
        {
            var email = new MailMessage(this.emailSettings.FromEmail, sendTo, subject, body);

            email.IsBodyHtml = isBodyHtml;

            var smtpClient = new SmtpClient(this.emailSettings.SmtpServer);

            smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;

            return smtpClient.SendMailAsync(email);
        }
    }
}