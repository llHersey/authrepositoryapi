using System.Threading.Tasks;
using APITemplate.Core.Model;

namespace APITemplate.Core.Interface
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetWithRoles(string email);
    }
}