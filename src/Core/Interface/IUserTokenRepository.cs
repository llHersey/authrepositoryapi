using System.Threading.Tasks;
using APITemplate.Core.Model;

namespace APITemplate.Core.Interface
{
    public interface IUserTokenRepository : IRepository<UserToken>
    {
        Task<UserToken> GetValidToken(int userId, byte[] hashToken);
    }
}