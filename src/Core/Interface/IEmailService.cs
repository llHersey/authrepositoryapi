using System.Collections.Generic;
using System.Threading.Tasks;

namespace APITemplate.Core.Interface
{
    public interface IEmailService
    {
        Task Send(string sendTo, string subject, string body, bool isBodyHtml);
    }
}