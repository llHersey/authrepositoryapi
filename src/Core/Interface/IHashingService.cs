namespace APITemplate.Core.Interface
{
    public interface IHashingService
    {
        byte[] Hash(byte[] salt, string passwordToHash);

        byte[] GenerateSalt();
        string GenerateToken(int length = 32);

        bool VerifyHash(string verify, byte[] salt, byte[] actual);
    }
}