using System.Threading.Tasks;

namespace APITemplate.Core.Interface
{
    public interface IUnitOfWork
    {
        Task<int> CompleteAsync();

        int Complete();
    }
}