using APITemplate.Core.Model;

namespace APITemplate.Core.Interface
{
    public interface IRoleRepository : IRepository<Role>
    {

    }
}