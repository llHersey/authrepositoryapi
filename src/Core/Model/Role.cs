using System.ComponentModel.DataAnnotations;

namespace APITemplate.Core.Model
{
    public class Role
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Description { get; set; }
    }
}