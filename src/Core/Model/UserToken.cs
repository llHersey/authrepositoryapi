using System;
using System.ComponentModel.DataAnnotations;

namespace APITemplate.Core.Model
{
    public class UserToken
    {
        public int Id { get; set; }

        [Required]
        public byte[] HashToken { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        public bool IsActive { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}