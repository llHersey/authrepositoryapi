using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using APITemplate.Controllers.Resource;
using APITemplate.Core.Interface;
using APITemplate.Core.Model;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace APITemplate.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IUserRepository userRepository;
        private readonly IUserTokenRepository userTokenRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IHashingService hashingService;
        private readonly IEmailService emailService;
        private readonly IConfiguration config;

        public AuthController(IMapper mapper, IUserRepository userRepository, IUserTokenRepository userTokenRepository, IUnitOfWork unitOfWork, IHashingService hashingService, IEmailService emailService, IConfiguration config)
        {
            this.mapper = mapper;
            this.userRepository = userRepository;
            this.userTokenRepository = userTokenRepository;
            this.unitOfWork = unitOfWork;
            this.hashingService = hashingService;
            this.emailService = emailService;
            this.config = config;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginResource login)
        {
            var user = await userRepository.GetWithRoles(login.Email);

            if (user is null)
                return Unauthorized();

            var isValidLogin = hashingService.VerifyHash(login.Password, user.Salt, user.Password);

            if (!isValidLogin)
                return Unauthorized();

            return Ok(new { Token = GenerateToken(user) });
        }


        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register(RegisterResource register)
        {
            register.Email = register.Email?.ToLower();

            if (await userRepository.ExistAsync(p => p.Email == register.Email))
                return BadRequest("Correo ya existe");

            var user = mapper.Map<User>(register);

            user.LastUpdate = DateTime.UtcNow;
            user.JoinDate = DateTime.UtcNow;

            user.Salt = hashingService.GenerateSalt();
            user.Password = hashingService.Hash(user.Salt, register.Password);

            await userRepository.AddAsync(user);
            await unitOfWork.CompleteAsync();

            user = await userRepository.GetWithRoles(user.Email);

            return Ok(new { Token = GenerateToken(user) });
        }


        [HttpPost("forget")]
        public async Task<IActionResult> GenerateForgetPassword([FromBody]string email)
        {
            email = email.ToLower();

            var user = await userRepository.GetAsync(ur => ur.Email == email);

            if (user is null)
                return Ok(); //Do not tell the user if the email existed or not

            var plainToken = hashingService.GenerateToken();
            var hashToken = hashingService.Hash(user.Salt, plainToken);
            var tokenValidationTime = Convert.ToInt32(config["Jwt:MinutesValidationPasswordToken"]);

            await userTokenRepository.AddAsync(new UserToken()
            {
                ExpirationDate = DateTime.UtcNow.AddMinutes(tokenValidationTime),
                HashToken = hashToken,
                UserId = user.Id
            });

            await unitOfWork.CompleteAsync();

            /* 
            UNNCOMENT THIS LINE IF YOU ALREADY CONFIGURED THE SMTP SERVER AND THE MAIL BODY
            await emailService.Send(user.Email, "Subject", "Body with token url", isBodyHtml:false ); 
            */

            return Ok(); //JUST FOR DEBUG PORPUSES, please remove this in a production enviroment
        }


        [HttpGet("forget")]
        public async Task<IActionResult> CheckToken(int u, string tk)
        {
            var userId = u;
            var plainToken = tk;

            var user = await userRepository.GetAsync(ur => ur.Id == userId);

            if (user is null)
                return BadRequest("Usuario no existe");

            var hashToken = hashingService.Hash(user.Salt, plainToken);
            var validUserToken = await userTokenRepository.GetValidToken(userId, hashToken);

            if (validUserToken is null)
                return BadRequest("El token es invalido, ya fué usado o ya expiró");

            return Ok();
        }

        [HttpPost("resetPass")]
        public async Task<IActionResult> ResetPassword(ResetPasswordResource resetPasswordResource)
        {
            var user = await userRepository.GetAsync(ur => ur.Id == resetPasswordResource.UserId);

            if (user is null)
                return BadRequest("Usuario no existe");

            var hashToken = hashingService.Hash(user.Salt, resetPasswordResource.Token);

            var validUserToken = await userTokenRepository.GetValidToken(user.Id, hashToken);

            if (validUserToken is null)
                return BadRequest("El token es invalido, ya fué usado o ya expiró");

            var newHash = hashingService.Hash(user.Salt, resetPasswordResource.Password);

            user.Password = newHash;
            validUserToken.IsActive = false;

            await unitOfWork.CompleteAsync();

            user = await userRepository.GetWithRoles(user.Email);

            return Ok(new { Token = GenerateToken(user) });
        }


        [HttpPost("changePass")]
        public async Task<IActionResult> ChangePassword(ChangePasswordResource changePasswordResource)
        {
            var user = await userRepository.GetAsync(ur => ur.Id == changePasswordResource.UserId);

            if (user is null)
                return BadRequest("Usuario no existe");

            var isValidPassword = hashingService.VerifyHash(changePasswordResource.OldPassword, user.Salt, user.Password);

            if (!isValidPassword)
                return BadRequest("Contraseña invalida");

            var newHash = hashingService.Hash(user.Salt, changePasswordResource.NewPassword);

            user.Password = newHash;

            await unitOfWork.CompleteAsync();

            user = await userRepository.GetWithRoles(user.Email);

            return Ok(new { Token = GenerateToken(user) });
        }

        private string GenerateToken(User user)
        {

            var claims = new List<Claim>() {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.Email),
                new Claim("JoinDate", user.JoinDate.ToString("dd/MM/yyyy H:mm")),
                new Claim("LastUpdate", user.LastUpdate.ToString("dd/MM/yyyy H:mm"))
            };

            var roles = user.UserRoles.Select(ur => new Claim("roles", ur.Role.Description));

            claims.AddRange(roles);

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:Key"]));
            var signInCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            var tokeOptions = new JwtSecurityToken(
                issuer: config["Jwt:Issuer"],
                audience: config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.UtcNow.AddHours(24),
                signingCredentials: signInCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(tokeOptions);
        }
    }
}