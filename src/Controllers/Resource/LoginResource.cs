using System.ComponentModel.DataAnnotations;

namespace APITemplate.Controllers.Resource
{
    public class LoginResource
    {
        [Required(ErrorMessage = "Este campo es requerido")]
        [EmailAddress(ErrorMessage = "Tiene que ser una dirección de correo válida")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Este campo es requerido")]
        [StringLength(18, MinimumLength = 5, ErrorMessage = "La contraseña tiene que estar entre 5 y 18 caracteres")]
        public string Password { get; set; }
    }
}