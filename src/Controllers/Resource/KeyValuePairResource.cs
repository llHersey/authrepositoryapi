namespace APITemplate.Controllers.Resource
{
    public class KeyValuePairResource
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}