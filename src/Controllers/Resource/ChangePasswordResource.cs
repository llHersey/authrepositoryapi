namespace APITemplate.Controllers.Resource
{
    public class ChangePasswordResource
    {
        public int UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}