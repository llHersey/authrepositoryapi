using System.Collections.Generic;
using System.Threading.Tasks;
using APITemplate.Controllers.Resource;
using APITemplate.Core.Interface;
using APITemplate.Core.Model;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APITemplate.Controllers
{
    [Route("api/roles")]
    [Authorize]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IRoleRepository roleRepository;

        public RoleController(IMapper mapper, IRoleRepository roleRepository)
        {
            this.mapper = mapper;
            this.roleRepository = roleRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ICollection<KeyValuePairResource>> GetMakes()
        {
            var roles = await roleRepository.GetAllAsync();
            return mapper.Map<ICollection<Role>, List<KeyValuePairResource>>(roles);
        }
    }
}