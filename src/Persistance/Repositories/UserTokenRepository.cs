using System;
using System.Linq;
using System.Threading.Tasks;
using APITemplate.Core.Interface;
using APITemplate.Core.Model;

namespace APITemplate.Persistance.Repositories
{
    public class UserTokenRepository : Repository<UserToken>, IUserTokenRepository
    {
        private readonly Context context;

        public UserTokenRepository(Context context) : base(context)
        {
            this.context = context;
        }

        public Task<UserToken> GetValidToken(int userId, byte[] hashToken)
        {
            return GetAsync(utr =>
                        utr.ExpirationDate > DateTime.UtcNow &&
                        utr.IsActive &&
                        utr.UserId == userId &&
                        utr.HashToken.SequenceEqual(hashToken));

        }
    }
}