using APITemplate.Core.Interface;
using APITemplate.Core.Model;

namespace APITemplate.Persistance.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        private readonly Context context;
        public RoleRepository(Context context) : base(context)
        {
            this.context = context;
        }
    }
}