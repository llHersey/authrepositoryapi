using System.Threading.Tasks;
using APITemplate.Core.Interface;
using APITemplate.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace APITemplate.Persistance.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly Context context;

        public UserRepository(Context context) : base(context)
        {
            this.context = context;
        }

        public Task<User> GetWithRoles(string email)
        {
            return this.context.Users
                .Include(us => us.UserRoles)
                    .ThenInclude(ur => ur.Role)
                .FirstOrDefaultAsync(us => us.Email == email);
        }
    }
}