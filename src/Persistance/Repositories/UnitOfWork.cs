using System.Threading.Tasks;
using APITemplate.Core.Interface;

namespace APITemplate.Persistance.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Context context;

        public UnitOfWork(Context context)
        {
            this.context = context;
        }
        public int Complete()
        {
            return this.context.SaveChanges();
        }

        public Task<int> CompleteAsync()
        {
            return this.context.SaveChangesAsync();
        }
    }
}