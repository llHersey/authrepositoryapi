using APITemplate.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace APITemplate.Persistance
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }
        public Context(DbContextOptions<Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasKey(ur => new { ur.UserId, ur.RoleId });
            modelBuilder.Entity<UserToken>().Property(ut => ut.IsActive).HasDefaultValue(true);
            modelBuilder.Entity<User>().Property(ut => ut.IsActive).HasDefaultValue(true);
        }
    }
}