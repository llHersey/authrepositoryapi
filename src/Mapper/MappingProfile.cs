using System.Linq;
using AutoMapper;
using APITemplate.Controllers.Resource;
using APITemplate.Core.Model;

namespace InnovationAPI.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Resource to API
            CreateMap<RegisterResource, User>()
                .ForMember(u => u.IsActive, opt => opt.Ignore())
                .ForMember(u => u.Id, opt => opt.Ignore())
                .ForMember(u => u.JoinDate, opt => opt.Ignore())
                .ForMember(u => u.LastUpdate, opt => opt.Ignore())
                .ForMember(u => u.Password, opt => opt.Ignore())
                .ForMember(u => u.Salt, opt => opt.Ignore())
                .AfterMap((ur, us) =>
                {

                    //Removed unused roles
                    var removedRoles = us.UserRoles.Where(r => !ur.Roles.Contains(r.RoleId));
                    foreach (var item in removedRoles)
                        us.UserRoles.Remove(item);

                    //Add new roles
                    var newRoles = ur.Roles.Where(id => !us.UserRoles.Any(r => r.RoleId == id)).Select(id => new UserRole { RoleId = id });
                    foreach (var item in newRoles)
                        us.UserRoles.Add(item);
                });

        }
    }
}